from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from colorama import init as colorama_init
from colorama import Fore
from colorama import Style
import random
import time

# Open the browser
driver = webdriver.Chrome()

# Init text color
colorama_init()

# Go to the target page
# driver.get("https://www.tesla.com/")
driver.get("https://www.tesla.com/zh_tw")

# Wait for the page to load partially
time.sleep(5)  # Introduce a short delay to allow initial page loading

# Try finding the "Design" menu using explicit waiting
try:
    wait_time = 5  # Set a shorter wait timeout for visibility checks
    # 
    # //a[.//span[text()='New Database/Server']]"
    design_menu = WebDriverWait(driver, wait_time).until(
        EC.visibility_of_element_located((By.XPATH, "//a[.//span[text()='立即訂購']]"))
    )
except (TimeoutException, NoSuchElementException):
    # If not found immediately, handle the exception
    print("Failed to find the 'Design' menu using explicit waiting.")
    pass  # Skip clicking if not found initially

# If the menu was found using explicit waiting, click it
if design_menu:
    design_menu.click()

wait_time = 10  # Set a wait timeout in seconds
WebDriverWait(driver, wait_time).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, "*[href]")))

# Identify clickable elements using CSS selectors
clickable_elements = driver.find_elements(By.CSS_SELECTOR, "*[href]")

# Extract and display element information
for element in clickable_elements:
    element_text = element.text.strip()
    element_url = element.get_attribute("href")
    print(f"Clickable Element: {element_text}")
    print(f"URL: {element_url}")
    print("-" * 20)
    print(f"This URL connect is {Fore.GREEN}PASSED{Style.RESET_ALL}!")


# Get the new page URL (if clicked)
if design_menu:
    new_page_url = driver.current_url
    print(f"Clicked page URL: {new_page_url}")

    # Get the new page title (if clicked)
    new_page_title = driver.title
    print(f"Clicked page title: {new_page_title}")

    # Get the new page content (if clicked)
    # new_page_content = driver.page_source
    # print("Clicked page content:")
    # print(new_page_content)
    print(f"This selenium test is {Fore.GREEN}PASSED{Style.RESET_ALL}!")

# Close the browser
driver.quit()
