from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException
import csv

# Open the browser and navigate to the page
driver = webdriver.Chrome()
driver.get("https://www.tesla.com/model3/design#overview")

# Wait for the page to load fully
wait_time = 10  # Set a wait timeout in seconds
WebDriverWait(driver, wait_time).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, "*[href]")))

# Identify clickable elements using CSS selectors
clickable_elements = driver.find_elements(By.CSS_SELECTOR, "*[href]")

# Extract element text and URLs
clickable_elements_text = []
clickable_elements_url = []
for element in clickable_elements:
    clickable_elements_text.append(element.text.strip())
    clickable_elements_url.append(element.get_attribute("href"))

# Create and write to CSV file
with open('model3_design_elements.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['Element Text', 'URL'])
    for element_text, element_url in zip(clickable_elements_text, clickable_elements_url):
        writer.writerow([element_text, element_url])
